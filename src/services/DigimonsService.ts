import { DigimonI } from "../interfaces/DigimonInterfaces";
const db = require('../db/Digimons.json');

module DigimonsService { 
    export function getAll(): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        return digimons
    }
    export function get(id: number): DigimonI {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = digimons.filter(e => e.id === id);
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon[0];
    }
    export function getByName(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        const matches: Array<DigimonI> = digimons.filter(function(el) {
            return el.name.toLowerCase().indexOf(name.toLowerCase()) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];
        digimons.forEach(digimon => {
            const found = digimon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(digimon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        }
        return matches;
    }

    export function getByWeak(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];
        const found = digimons.filter(e => e.name.toLowerCase() === name.toLowerCase());
        const type = found[0].type.filter(e => e.name);
        digimons.forEach(digimon => {
            const weak = digimon.type.filter(e => e.strongAgainst === type[0].name);
            if (weak.length>0) {
                matches.push(digimon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }

    export function getByStrong(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];
        const found = digimons.filter(e => e.name.toLowerCase() === name.toLowerCase());
        const type = found[0].type.filter(e => e.name);
        digimons.forEach(digimon => {
            const strong = digimon.type.filter(e => e.weakAgainst === type[0].name);
            if (strong.length>0) {
                matches.push(digimon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }

    export function create(newDigimon: DigimonI){
        db.push(newDigimon);
        return newDigimon;
    }
}

export default DigimonsService;
